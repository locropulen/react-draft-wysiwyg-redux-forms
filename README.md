# react-draft-wysiwyg-redux-forms

A simple way to handle [react-draft-wysiwyg](https://jpuri.github.io/react-draft-wysiwyg/#/docs?_k=jjqinp) as field in [redux-form](https://redux-form.com/7.0.4/) using [Editor State](https://draftjs.org/docs/api-reference-editor-state.html#content)

## package.json
```json
{
  "name": "react-draft-wysiwyg-redux-forms",
  "version": "0.1.0",
  "private": true,
  "dependencies": {
    "react": "^16.0.0",
    "react-dom": "^16.0.0",
    "react-redux": "^5.0.6",
    "react-scripts": "1.0.14",
    "redux": "^3.7.2",
    "redux-form": "^7.0.4",
    "redux-logger": "^3.0.6"
  }
}
```
![](assets/692d5d58e8.gif?raw=true)

## More info

See redux-form [getting started guide](https://redux-form.com/7.0.4/docs/gettingstarted.md/)

## CRNA
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## License