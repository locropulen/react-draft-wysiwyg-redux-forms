import React, { Component } from 'react';

import './App.css';

import Form from './Form';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
           <h1 className="App-title">Welcome to react-draft-wysiwyg-redux-forms</h1>
        </header>
        <Form />
        <footer />
      </div>
    );
  }
}

export default App;
