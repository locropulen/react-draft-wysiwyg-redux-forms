import React from 'react'
import { reduxForm } from 'redux-form'
import EditorField from './EditorField';

const handleForm = (values) => {
  alert(`Awesome editor text value is ${values.editorText}!`);
}
const Form = props => {
  const { handleSubmit } = props

  return (
    <form onSubmit={handleSubmit(handleForm)}>
      <button key='submit' type='submit'>Submit</button>
      <EditorField
        key='field'
        name='editorText'
        id='inputEditorText'
        disabled={false}
        placeholder='Type here'
    />
    </form>
  )
}

export default reduxForm({
  // a unique name for the form
  form: 'draft'
})(Form);
